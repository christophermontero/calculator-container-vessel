const express = require('express');
const cors =require('cors');
const app = express();
const bodyParser = require('body-parser');
const port = 3000;

// Enable cors
app.use(express.json())
app.use(cors({
	origin: '*', // Route to make the requests
	credentials: true
}));

// Parse aplication/json
app.use(bodyParser.json());

// Multiply's API is called post method
app.post('/multiplicar', (peticion,respuesta) => { // Declare a route for consume the API
    let number1 = peticion.body.number1;
    let number2 = peticion.body.number2;

    if ( isNaN(number1) || isNaN(number2) ){
        respuesta.status(400).json({message:'Números inválidos'});
    } else {
        let multiplicar = parseFloat(number1) * parseFloat(number2);
        respuesta.json({resultado : multiplicar});
    }
});

// Raise the server in port 3000
app.listen(port, () => {
    console.log(`Servidor activo en http://localhost:${port}`)
})
