FROM node:14.9.0-alpine3.10
RUN mkdir -p /node/multiply-api
WORKDIR /node/multiply-api
COPY . .
RUN npm install
EXPOSE 3000
CMD ["node", "index.js"]
